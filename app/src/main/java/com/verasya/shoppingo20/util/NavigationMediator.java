package com.verasya.shoppingo20.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NavigationMediator {

    public NavigationMediator(){}

    public void startActivityWithFlag(Activity mActivity, Class<?> cls){
        Intent intent = new Intent(mActivity, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.startActivity(intent);

    }

    public void startActivity(Activity mActivity, Class<?> cls){
        Intent intent = new Intent(mActivity, cls);
        mActivity.startActivity(intent);
    }

    public void startActivityWithBundles(Activity mActivity, Class<?> cls, Bundle extras, Bundle activityOptions){
        Intent intent = new Intent(mActivity, cls);
        if(extras != null){
            intent.putExtras(extras);
        }
        mActivity.startActivity(intent, activityOptions);
    }
}
