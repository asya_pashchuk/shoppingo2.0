package com.verasya.shoppingo20.activity;

import android.app.Activity;

import com.verasya.shoppingo20.dialog.MessageFragmentDialog;
import com.verasya.shoppingo20.util.NavigationMediator;

public class BaseActivity extends Activity{

    private NavigationMediator navigationMediator = new NavigationMediator();

    public NavigationMediator getNavigationMediator() {
        return navigationMediator;
    }

    public void showMessage(String message) {
        MessageFragmentDialog.newInstance(message).show(getFragmentManager(), null);
    }

    protected void showMessage(int messageRes) {
        MessageFragmentDialog.newInstance(getString(messageRes)).show(getFragmentManager(), null);
    }
}
