package com.verasya.shoppingo20.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ShoppinGoSQLiteOpenHelper extends SQLiteOpenHelper {
    public ShoppinGoSQLiteOpenHelper(Context context) {
        super(context, ShoppinGoDbDescriptor.DATABASAE_NAME, null, ShoppinGoDbDescriptor.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
