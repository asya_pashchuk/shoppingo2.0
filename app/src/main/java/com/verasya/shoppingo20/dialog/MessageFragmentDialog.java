package com.verasya.shoppingo20.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class MessageFragmentDialog extends DialogFragment{

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String BUTTON_TEXT = "BUTTON_TEXT";

    public static MessageFragmentDialog newInstance(String message) {
        return newInstance(null, message, null);
    }

    public static MessageFragmentDialog newInstance(String title, String message, String buttonText) {

        MessageFragmentDialog dialog = new MessageFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE,title);
        bundle.putString(MESSAGE,message);
        bundle.putString(BUTTON_TEXT,buttonText);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
        Bundle arguments = getArguments();
        String title = arguments.getString(TITLE);

        if(title != null){
            build.setTitle(title);
        }
        build.setMessage(arguments.getString(MESSAGE));

        String textFromArguments = arguments.getString(BUTTON_TEXT);
        String buttonText = textFromArguments == null
                ? "OK"
                : textFromArguments;

        build.setPositiveButton(buttonText, null);
        return build.create();
    }
}
